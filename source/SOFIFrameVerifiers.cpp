/*
 Copyright 2008-2014 Peter Dedecker.
 
 This file is part of Localizer.
 
 Localizer is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 Localizer is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with Localizer.  If not, see <http://www.gnu.org/licenses/>.
 
 
 Additional permission under GNU GPL version 3 section 7
 
 If you modify this Program, or any covered work, by
 linking or combining it with libraries required for interaction
 with analysis programs such as Igor Pro or Matlab, or to acquire
 data from or control hardware related to an experimental measurement,
 the licensors of this Program grant you additional permission
 to convey the resulting work.
 */

#include "SOFIFrameVerifiers.h"

SOFIFrameVerifier_NoSaturation::SOFIFrameVerifier_NoSaturation(LocalizerStorageType storageType_rhs) {
	
	validLimitsSet = true;
	switch (storageType_rhs) {
            // some systems (e.g. the Nikon TIRF) never report absolutely saturated pixel
            // values, but instead report a different value close to it
            // so add in some arbitrary margin for the saturation detection
		case kInt8:
			this->saturationValue = 127.0 - 1.0;
			break;
		case kUInt8:
			this->saturationValue = 255.0 - 2.0;
			break;
		case kInt16:
			this->saturationValue = 32767.0 - 200.0;
			break;
		case kUInt16:
			this->saturationValue = 65535.0 - 200.0;
			break;
		case kInt32:
			this->saturationValue = 2147483647.0 - 500.0;
			break;
		case kUInt32:
			this->saturationValue = 4294967295.0 - 500.0;
			break;
		case kInt64:
			this->saturationValue = 9223372036854775807.0 - 1000.0;
			break;
		case kUInt64:
			this->saturationValue = 18446744073709551615.0 - 1000.0;
			break;
		default:
			validLimitsSet = false;
			break;
	}
}

bool SOFIFrameVerifier_NoSaturation::isValidFrame(ImagePtr frame) const {
	if (validLimitsSet == false)
		return true;
	
	size_t nRows = frame->rows();
	size_t nCols = frame->cols();
	
	size_t nPixels = nRows * nCols;
	double localSaturationValue = this->saturationValue;
	
	double *dataPtr = frame->data();
	for (size_t i = 0; i < nPixels; i+=1) {
		if (dataPtr[i] >= localSaturationValue) {
			return false;
		}
	}
	
	return true;
}

bool SOFIFrameVerifier_MaxPixelValue::isValidFrame(ImagePtr image) const {
	size_t nRows = image->rows();
	size_t nCols = image->cols();
	
	size_t nPixels = nRows * nCols;
	double localMaxPixelValue = this->maxPixelValue;
	
	double *dataPtr = image->data();
	for (size_t i = 0; i < nPixels; i+=1) {
		if (dataPtr[i] > localMaxPixelValue) {
			return false;
		}
	}
	
	return true;
}
